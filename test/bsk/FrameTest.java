package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.*;


public class FrameTest {

	@Test
	public void testGetFirstThrowOfAFrame() throws BowlingException{
		Frame frame = new Frame(2, 4);
		
		assertEquals(2, frame.getFirstThrow());
		
	}
	
	@Test
	public void testGetSecondThrowOfAFrame() throws BowlingException{
		Frame frame = new Frame(2, 4);
		
		assertEquals(4, frame.getSecondThrow());
		
	}
	
	@Test
	public void testGetScoreOfAFrame() throws BowlingException{
		Frame frame = new Frame(2, 4);
		
		assertEquals(6, frame.getScore());
		
	}
	
	@Test
	public void testGetFrameAt() throws BowlingException{
		Game game = new Game();
		Frame f1;
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(2, 5));
		game.addFrame(new Frame(1, 1));
		game.addFrame(f1 = new Frame(4, 2));
		game.addFrame(new Frame(8, 0));
		game.addFrame(new Frame(2, 3));
		game.addFrame(new Frame(1, 3));
		game.addFrame(new Frame(1, 6));
		game.addFrame(new Frame(2, 0));
		game.addFrame(new Frame(8, 1));
		assertEquals( f1, game.getFrameAt(3));
		
	}
	
	@Test
	public void testCalculateScore() throws BowlingException{
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(2, 5));
		game.addFrame(new Frame(1, 1));
		game.addFrame(new Frame(4, 2));
		game.addFrame(new Frame(8, 0));
		game.addFrame(new Frame(2, 3));
		game.addFrame(new Frame(1, 3));
		game.addFrame(new Frame(1, 6));
		game.addFrame(new Frame(2, 0));
		game.addFrame(new Frame(8, 1));
		
		assertEquals(56, game.calculateScore());
	}
	
	@Test
	public void testGetBonusOfAFrame() throws BowlingException{
		Frame frame = new Frame(8,2);
		
		frame.setBonus(2);
		assertEquals(2, frame.getBonus());
	}
	
	@Test
	public void testIfFrameIsSpare() throws BowlingException{
		Frame frame = new Frame(8,2);
		assertTrue(frame.isSpare());
	}
	
	@Test
	public void testFrameScoreIfSpareAndBonusIsApplied() throws BowlingException{
		Frame frame = new Frame(8,2);
		frame.setBonus(2);
		assertEquals(12, frame.getScore());
	}
	
	@Test
	public void testTotalScoreOfAGameIfASpareFrameIsPresent() throws BowlingException{
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(2, 5));
		game.addFrame(new Frame(1, 1));
		game.addFrame(new Frame(4, 2));
		game.addFrame(new Frame(8, 2));
		game.addFrame(new Frame(2, 3));
		game.addFrame(new Frame(1, 3));
		game.addFrame(new Frame(1, 6));
		game.addFrame(new Frame(2, 0));
		game.addFrame(new Frame(8, 1));
		
		assertEquals(60, game.calculateScore());
	}
	
	@Test
	public void testIfFrameIsStrike() throws BowlingException{
		Frame frame = new Frame(10,0);
		assertTrue(frame.isStrike());
	}
	
	@Test
	public void testFrameScoreIfStrikeAndBonusIsApplied() throws BowlingException{
		Frame frame = new Frame(10,0);
		frame.setBonus(8);
		assertEquals(18, frame.getScore());
	}
	
	@Test
	public void testTotalScoreOfAGameIfAStrikeFrameIsPresent() throws BowlingException{
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(2, 5));
		game.addFrame(new Frame(1, 1));
		game.addFrame(new Frame(4, 2));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(2, 3));
		game.addFrame(new Frame(1, 3));
		game.addFrame(new Frame(1, 6));
		game.addFrame(new Frame(2, 0));
		game.addFrame(new Frame(8, 1));
		
		assertEquals(63, game.calculateScore());
	}
	
	@Test
	public void testTotalScoreOfAGameIfAStrikeFrameIsPresentAndFollowedByASpare() throws BowlingException{
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(2, 5));
		game.addFrame(new Frame(1, 1));
		game.addFrame(new Frame(4, 2));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(2, 8));
		game.addFrame(new Frame(1, 3));
		game.addFrame(new Frame(1, 6));
		game.addFrame(new Frame(2, 0));
		game.addFrame(new Frame(8, 1));
		
		assertEquals(74, game.calculateScore());
	}
	
	@Test
	public void testTotalScoreOfAGameIfMultipleStrikesFollowEachOther() throws BowlingException{
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(2, 5));
		game.addFrame(new Frame(1, 1));
		game.addFrame(new Frame(4, 2));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(1, 3));
		game.addFrame(new Frame(1, 6));
		game.addFrame(new Frame(2, 0));
		game.addFrame(new Frame(8, 1));
		
		assertEquals(78, game.calculateScore());
	}
	
	@Test
	public void testTotalScoreOfAGameIfMultipleSparesFollowEachOther() throws BowlingException{
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(2, 5));
		game.addFrame(new Frame(1, 1));
		game.addFrame(new Frame(4, 2));
		game.addFrame(new Frame(8, 2));
		game.addFrame(new Frame(7, 3));
		game.addFrame(new Frame(1, 3));
		game.addFrame(new Frame(1, 6));
		game.addFrame(new Frame(2, 0));
		game.addFrame(new Frame(8, 1));
		
		assertEquals(71, game.calculateScore());
	}
	
	@Test
	public void testGameEndingWithASpareGivingAnExtraThrow() throws BowlingException{
		Game game = new Game();
		game.setFirstBonusThrow(7);
		
		assertEquals(7, game.getFirstBonusThrow());
	}
	
	@Test
	public void testTotalScoreOfAGameIfGameEndsWithSpare() throws BowlingException{
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(2, 5));
		game.addFrame(new Frame(1, 1));
		game.addFrame(new Frame(4, 2));
		game.addFrame(new Frame(8, 0));
		game.addFrame(new Frame(2, 3));
		game.addFrame(new Frame(1, 3));
		game.addFrame(new Frame(1, 6));
		game.addFrame(new Frame(2, 0));
		game.addFrame(new Frame(8, 2));
		
		game.setFirstBonusThrow(7);
		
		assertEquals(64, game.calculateScore());
	}
	
	@Test
	public void testGameEndingWithAStrikeGivingAnotherExtraThrow() throws BowlingException{
		Game game = new Game();
		game.setSecondBonusThrow(7);
		
		assertEquals(7, game.getSecondBonusThrow());
	}
	
	@Test
	public void testTotalScoreOfAGameIfGameEndsWithStrike() throws BowlingException{
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(2, 5));
		game.addFrame(new Frame(1, 1));
		game.addFrame(new Frame(4, 2));
		game.addFrame(new Frame(8, 0));
		game.addFrame(new Frame(2, 3));
		game.addFrame(new Frame(1, 3));
		game.addFrame(new Frame(1, 6));
		game.addFrame(new Frame(2, 0));
		game.addFrame(new Frame(10, 0));
		
		game.setFirstBonusThrow(7);
		game.setSecondBonusThrow(7);
		
		assertEquals(71, game.calculateScore());
	}
	
	@Test
	public void testTotalScoreOfAGameIfGameIsAllStrikesAndBonusThrowsAlsoStrikes() throws BowlingException{
		Game game = new Game();
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		
		game.setFirstBonusThrow(10);
		game.setSecondBonusThrow(10);
		
		assertEquals(300, game.calculateScore());
	}


}
