package tdd.training.bsk;

public class Game {
	
	private Frame[] frames;
	private int framesArrayIndexToAddTo = 0;
	private int firstBonusThrow;
	private int secondBonusThrow;
	private final static int MAX_GAME_THROWS = 9;

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() {
		frames = new Frame[10];
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		
		this.frames[framesArrayIndexToAddTo] = frame;
		framesArrayIndexToAddTo++;
		
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return frames[index];	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		this.firstBonusThrow = firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		this.secondBonusThrow = secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		// To be implemented
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		// To be implemented
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		
		int calculatedScore = 0;
		for ( int i = 0; i < 10; i++ ) {
			
			if( frames[i].isSpare() && i < MAX_GAME_THROWS ) {
				
				frames[i].setBonus( frames[i+1].getFirstThrow() );
				
			}else if( frames[i].isStrike() && i < MAX_GAME_THROWS ) {
				
				if ( frames[i+1].isStrike() && i < MAX_GAME_THROWS - 1 ) {
					
					frames[i].setBonus( frames[i+1].getScore() + frames[i+2].getFirstThrow() );
					
				}else if ( frames[i+1].isStrike() && i == MAX_GAME_THROWS - 1 && firstBonusThrow == 10) {
				
					frames[i].setBonus( frames[i+1].getScore() + firstBonusThrow );
					
				}else {
					
					frames[i].setBonus( frames[i+1].getScore() );
					
				}
				
				
				
			}
			
			calculatedScore = calculatedScore + frames[i].getScore();
			
		}
		
		if( frames[MAX_GAME_THROWS].isSpare() ) {
			
			calculatedScore += firstBonusThrow;
			
		}else if( frames[MAX_GAME_THROWS].isStrike() ) {
			
			calculatedScore += firstBonusThrow + secondBonusThrow;
			
		}
		
		
		return calculatedScore;	
	}

}
